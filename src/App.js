import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import Users from './components/Users';
import UserForm from './components/Userform';
import Page2 from './components/Page2'
import store from './store';
import { Link } from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <div className="App">
            <header className="App-header">
              <Link  to="/">Page1</Link> | <Link  to="/page2">Page2</Link>
            </header>
            <Route
							exact
							path='/'
							render={(props) => (
								<React.Fragment>
                    <UserForm />
                  <hr />
                    <Users />
                  </React.Fragment>
            )}
						/>
            <Route path='/page2' component={Page2} />
          </div>
        </Router>
      </Provider>
    );
  }
}

export default App;

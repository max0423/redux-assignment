import { FETCH_USERS, NEW_USER, SELECT_USER, GET_USERS, REMOVE_USER } from './types';

const apiUrl = "https://jsonplaceholder.typicode.com/users";
export const fetchPosts = () => dispatch => {
  let users = {};

  if (sessionStorage.getItem('usersStorage')) {
    users =JSON.parse(sessionStorage.getItem('usersStorage'));
      dispatch({
      type: FETCH_USERS,
      payload: users
    })
 } else {
    fetch(apiUrl)
    .then(res => res.json())
    .then(users =>
      dispatch({
        type: FETCH_USERS,
        payload: users
      })
    );
  }
}

export const selectUser = (users, user) => dispatch => {
  dispatch({
    type: SELECT_USER,
    payload: {users, user}
  })
}

export const updatePost = (users, user) => dispatch => {

    let mockResult = user;
    fetch(apiUrl + "/" + user.id, {
      method: 'put',
      body: JSON.stringify({
      user
      })
    }).then((user) => {
      users.map((item)=>{
        if(item.id != mockResult.id){
           item
        } else{
          item.name = mockResult.name;
          item.email = mockResult.email; 
          item.phone = mockResult.phone; 
          item.address = {
            city: mockResult.address.city,
            street: mockResult.address.street
          },
          item.company = {
            name: mockResult.company.name
          
          }
        }
      })
      
      sessionStorage.setItem("usersStorage", JSON.stringify(users));
        dispatch({
          type: GET_USERS,
          payload: {users: users, user: mockResult}
        })
      })
   }


export const delUser =(users, id) => (dispatch)=>{
   fetch(apiUrl + '/' + id, {
    method: 'delete'
  })
  .then(response => {
           users = users.filter(elm => elm.id != id)
           dispatch(  {type: REMOVE_USER, payload: users});
        }
    );
}

export const createPost = postData => dispatch => {
    fetch(apiUrl, {
    method: 'post',
    headers: {
      'content-type': 'application/json'
    },
    body: JSON.stringify(postData)
  })
    .then(res => res.json())
    .then(user =>
      dispatch({
        type: NEW_USER,
        payload: user
      })
    );
};

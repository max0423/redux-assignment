

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchPosts, selectUser, updatePost } from '../actions/userActions';

class Page2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.selectUser? this.props.selectUser.name: "",
      email: this.props.selectUser? this.props.selectUser.email: "",
      phone:  this.props.selectUser ? this.props.selectUser.phone: "",
      street:    this.props.selectUser && this.props.selectUser.address ? this.props.selectUser.address.street: "",
      city:  this.props.selectUser &&  this.props.selectUser.address? this.props.selectUser.address.city: "",
      companyName:  this.props.selectUser? this.props.selectUser.company.name: "",
      id: this.props.selectUser ? this.props.selectUser.id: ""
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  componentWillMount() {
    //this.props.fetchPosts();
  }

  getDisabled = () => {
    return this.state.id === "" ? true : false
  }


  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  componentWillReceiveProps(nextProps) {
  }
  updateInputValue(e) {
    const value = e.target.value;
   
  }

 onSubmit = (e) => {
  e.preventDefault();
  const user = {
        id: this.props.selectUser.id,
        email: this.state.email,
        name: this.state.name,
        phone: this.state.phone,
        address: {
                  city: this.state.city,
                  street: this.state.street
                },
        company: { name : this.state.companyName }
  };
      this.props.updatePost(this.props.users, user);
  }

  render() {
    const user = this.props.selectUser;
    return (
      <form onSubmit={(e)=>this.onSubmit(e)}>
            <div  className="container">
     
            <div>
                  <label>
                      Name:
                      <input  type="text" 
                              value ={this.state.name}
                              name="name"
                              onChange={this.onChange}/>
                </label>
                  
            </div>
                  
            <div>
              <label>
                Email:
              <input  type="text" 
                  name="email"
                  value ={this.state.email}
                  onChange={this.onChange}/>
              </label>
              
            </div>

            <div>
              <label>
                Phone:
                <input  type="text" 
                    name="phone"
                    value ={this.state.phone}
                    onChange={this.onChange}/>       

              </label>
                
            </div>

            <div>
              <label>
                City:
                <input  type="text" 
                  name="city"
                  value ={this.state.city}
                  onChange={this.onChange}/> 
              </label>
            </div>
          
          
            <div>
              <label>
                Street:
                <input  type="text" 
                  name="street"
                  value ={this.state.street}
                  onChange={this.onChange}/> 
              </label>
            </div> 
            
            <div>
                <label>
                  Company:
                  <input  type="text" 
                    name="companyName"
                    value ={this.state.companyName}
                    onChange={this.onChange}/> 
                </label>

              </div>
         
        </div>
          <br />
          <input disabled ={this.getDisabled()}
             type="submit" 
             value="Update" />
      
      </form>
    );
  }
}


const mapStateToProps = state => ({
  selectUser: state.users.user,
  users: state.users.users
});

export default connect(mapStateToProps, {updatePost})(Page2);

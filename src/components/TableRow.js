import React, { Component } from 'react';
import { connect } from 'react-redux';
import { delUser} from '../actions/userActions';

class TableRow extends Component {
    constructor(props) {
        super(props);
        let { userlist } = this.props;
        this.state = {
            fields: {
              id: userlist.id
            }
          }
    }
    
  click(value) {
      this.props.handleChangess(this.state.fields.id, value);
  };
    render() {
        const { userlist , handleChangess, selectedId, rowIndex, usersList } = this.props;
       
      
        if( this.props.selectUser && this.props.selectUser.id > 0)
        {
          return (
       
          
          <tr key ={userlist.id}>
          <td key={`checkbox-${rowIndex}`}><input   checked={this.state.fields.id === this.props.selectUser.id} disabled={this.state.fields.id !== this.props.selectUser.id} type="checkbox" name="record" onChange={e => this.click(e.target.checked)} type="checkbox" name="record" /></td>
          <td key={`id-${rowIndex}`}>{userlist.id}</td> 
          <td key={`name-${rowIndex}`}>{userlist.name}</td> 
          <td key={`email-${rowIndex}`}>{userlist.email}</td> 
          <td key={`phone-${rowIndex}`}>{userlist.phone}</td> 
          <td key={`street-${rowIndex}`}>{ userlist.address  ? userlist.address.street: ''}</td> 
          <td key={`city-${rowIndex}`}>{userlist.address ? userlist.address.city: ''}</td> 
          <td key={`company-${rowIndex}`}>{userlist.company ? userlist.company.name : ''}</td> 
          <td> <button onClick={(e) =>this.props.delUser(this.props.users,1)} style={btnStyle}>x</button></td>
      </tr>
        )
        } else{
        return (
          
            
             <tr key ={userlist.id}>
                <td key={`checkbox-${rowIndex}`}><input disabled={selectedId && this.state.fields.id !== selectedId} type="checkbox" name="record" onChange={e => this.click(e.target.checked)} type="checkbox" name="record" /></td>
                <td key={`id-${rowIndex}`}>{userlist.id}</td> 
                <td key={`name-${rowIndex}`}>{userlist.name}</td> 
                <td key={`email-${rowIndex}`}>{userlist.email}</td> 
                <td key={`phone-${rowIndex}`}>{userlist.phone}</td> 
                <td key={`street-${rowIndex}`}>{ userlist.address  ? userlist.address.street: ''}</td> 
                <td key={`city-${rowIndex}`}>{userlist.address ? userlist.address.city: ''}</td> 
                <td key={`company-${rowIndex}`}>{userlist.company ? userlist.company.name : ''}</td> 
                <td> <button onClick={(e) =>this.props.delUser(this.props.users, userlist.id)} style={btnStyle}>x</button></td>
            </tr> 
          
        )
        }
    }
}


const btnStyle = {
  background: '#ff0000',
  color: '#fff',
  border: 'none',
  padding: '5px 9px',
  borderRadius: '50%',
  cursor: 'pointer',
  float: 'right'
}
const mapStateToProps = state => ({
   selectUser: state.users.user,
   users: state.users.items
 });

export default connect(mapStateToProps, {delUser})(TableRow);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createPost } from '../actions/userActions';

class UserForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      name: '',
      phone: '',
      city: '',
      street: '',
      companyName: ''
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  onSubmit(e) {
    e.preventDefault();
    let address ={};

    const user = {
      email: this.state.email,
      name: this.state.name,
      phone: this.state.phone,
      address: {
                city: this.state.city,
                street: this.state.street},

      company: {
        name : this.state.companyName}
    };

    this.props.createPost(user);
  }

  render() {
    return (
      <div>
      
        <form onSubmit={this.onSubmit}>
          <div  className="container">
          <div>
            <label>Title: </label>
            <br />
            <input
              type="text"
              name="name"
              onChange={this.onChange}
              value={this.state.name}
            />
          </div>
    
          <div>
            <label>Email: </label>
            <br />
            <input
              name="email"
              type="text"
              onChange={this.onChange}
              value={this.state.email}
            />
          </div>
         
          <div>
            <label>Phone: </label>
            <br />
            <input
              type="text"
              name="phone"
              onChange={this.onChange}
              value={this.state.phone}
            />
          </div>
          <div>
            <label>Street: </label>
            <br />
            <input
              type="text"
              name="street"
              onChange={this.onChange}
              value={this.state.street}
            />
          </div>
          <div>
            <label>City: </label>
            <br />
            <input
              type="text"
              name="city"
              onChange={this.onChange}
              value={this.state.city}
            />
          </div>
          <div>
            <label>Company Name: </label>
            <br />
            <input
              type="text"
              name="companyName"
              onChange={this.onChange}
              value={this.state.companyName}
            />
          </div>
          </div>
          <br />
          <button type="submit">Add User</button>
        </form>
        <br/>
      
      </div>
    );
  }
}

UserForm.propTypes = {
  createPost: PropTypes.func.isRequired
};

export default connect(null, { createPost })(UserForm);

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { fetchPosts, selectUser } from '../actions/userActions';
import  Tablerow  from './TableRow';


class Users extends Component {
 
  constructor(props){
    super(props);
 
    this.state = {
      selectedId: null,
    }
    this.handleChangess = this.handleChangess.bind(this);

    let list1 =  this.props.users;
  }

  componentWillMount() {
    this.props.fetchPosts();
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.newUser) {
      this.props.users.push(nextProps.newUser);
    }
  }

  handleChangess(id, value) { 
      this.setState({selectedId: value===true?id:null});
      if(value){
        this.props.selectUser(this.props.users, this.props.users[id-1])
      } else{
        this.props.selectUser(this.props.users, null)
      }
    }
  render() {
  //  let list1 =  this.props.users;
    return (
      <React.Fragment>
        <h4>Users</h4>
      <table>
          <tbody>
            {
                 this.props.users.map((data, rowIndex, users)=>{
                return <Tablerow key={rowIndex} rowIndex={rowIndex} usersList={users}  selectedId={this.state.selectedId && this.state.selectedId !== data.id} userlist={data} handleChangess={this.handleChangess} />
                })
            }     
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

Users.propTypes = {
  fetchPosts: PropTypes.func.isRequired,
  users: PropTypes.array.isRequired,
  newUser: PropTypes.object
};

const mapStateToProps = state => ({
  users: state.users.items,
  newUser: state.users.item,
 });

export default connect(mapStateToProps, { fetchPosts, selectUser })(Users);
